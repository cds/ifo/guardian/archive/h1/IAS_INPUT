# IAS_INPUT.py
#
# $Id: IAS_INPUT.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IAS_INPUT.py $
#

# [QUESTION] do we need to set the node ????

from guardian import GuardState

from ISC_GEN_STATES import gen_REFL_WFS_CENTERING

from ISC_DOF import\
			DOWN,\
			gen_LOCK_DOF,\
			SET_INPUT_ALIGN,\
			INPUT_ALIGN,\
			OFFLOAD_INPUT_ALIGN,\
			gen_SAVE_ALIGNMENT_VALUES

# LOCK_XARM
##################################################
LOCK_XARM_IR = gen_LOCK_DOF()
LOCK_XARM_IR.dof = 'XARM_IR'


class XARM_LOCKED(GuardState):
	request =True
	def main(self):
		return True

# REFL_WFS_CENTERING_XARM
##################################################
REFL_WFS_CENTERING_XARM = gen_REFL_WFS_CENTERING('XARM')


# SAVE_INPUT_ALIGNMENT
##################################################
SAVE_INPUT_ALIGNMENT = gen_SAVE_ALIGNMENT_VALUES('XARM')
SAVE_INPUT_ALIGNMENT.optics = ['PR2']

##################################################

edges = [
#XARM IR edges:
    ('DOWN', 'LOCK_XARM_IR'),
    ('LOCK_XARM_IR', 'XARM_LOCKED'),
	('XARM_LOCKED', 'REFL_WFS_CENTERING_XARM'),
    ('REFL_WFS_CENTERING_XARM', 'SET_INPUT_ALIGN'), 
    ('SET_INPUT_ALIGN', 'INPUT_ALIGN'),
    ('INPUT_ALIGN', 'OFFLOAD_INPUT_ALIGN'),
    ('OFFLOAD_INPUT_ALIGN', 'SAVE_INPUT_ALIGNMENT'),
    ('SAVE_INPUT_ALIGNMENT', 'LOCK_XARM_IR'),
   ]


